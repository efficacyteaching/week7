package uk.ac.uos.i2j.week7.list;

import uk.ac.uos.i2j.week7.Block;

public interface ListBlock extends Block {
	void setParent(ListBlock block);
	void remove(ListBlock child);
}
