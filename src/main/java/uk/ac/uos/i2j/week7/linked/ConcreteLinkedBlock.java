package uk.ac.uos.i2j.week7.linked;

import uk.ac.uos.i2j.week7.Block;
import uk.ac.uos.i2j.week7.Format;

public class ConcreteLinkedBlock implements LinkedBlock {
	private String text;
	private int attributes;
	protected LinkedBlock parent;

	protected LinkedBlock first;
	protected LinkedBlock prev;
	protected LinkedBlock next;
	
	public ConcreteLinkedBlock(String text, int attributes, LinkedBlock parent) {
		this.text = text;
		this.attributes = attributes;
		this.parent = parent;
		this.first = null;
		this.prev = null;
		this.next = null;
	}
	
	public ConcreteLinkedBlock(String text, LinkedBlock parent) {
		this(text, Block.PLAIN, parent);
	}
	
	public ConcreteLinkedBlock(String text) {
		this(text, Block.PLAIN, null);
	}
	
	public ConcreteLinkedBlock() {
		this("");
	}
	
	public void setParent(LinkedBlock block) { parent = block; }
	public LinkedBlock getParent() { return parent; }
	public void setFirst(LinkedBlock block) { first = block; }
	public LinkedBlock getFirst() { return first; }
	public void setPrev(LinkedBlock block) { prev = block; }
	public LinkedBlock getPrev() { return prev; }
	public void setNext(LinkedBlock block) { next = block; }
	public LinkedBlock getNext() { return next; }
	
	@Override
	public void setAttribute(int attribute) {
		if (isLeaf()) {
			this.attributes |= attribute;
		} else {
			LinkedBlock block = getFirst();
			while (block != null) {
				block.setAttribute(attribute);
				block = block.getNext();
			}
		}
	}

	@Override
	public void clearAttribute(int attribute) {
		if (isLeaf()) {
			this.attributes ^= attribute;
		} else {
			LinkedBlock block = getFirst();
			while (block != null) {
				block.clearAttribute(attribute);
				block = block.getNext();
			}
		}
	}

	@Override
	public boolean hasAttribute(int attribute) {
		return 0 != (this.attributes & attribute);
	}
	
	@Override
	public Block findBlock(int pos) {
		if (pos < 0 || pos >= length()) return null;
		int start = 0;
		int end = 0;
		LinkedBlock block = getFirst();
		while (block != null) {
			end = start + block.length();
			if (pos >= start && pos < end) {
				if (block.isLeaf()) {
					return block;
				}
				return block.findBlock(pos-start);
			}
			start = end;
			block = block.getNext();
		}
		return null;
	}
	

	@Override
	public void split(int pos) {
		if (pos < 0 || pos >= length()) return;
		if (isLeaf()) {
			ConcreteLinkedBlock before = new ConcreteLinkedBlock(this.text.substring(0, pos), this.attributes, this); 
			ConcreteLinkedBlock after = new ConcreteLinkedBlock(this.text.substring(pos), this.attributes, this);
			first = before;
			before.next = after;
			after.prev = before;
			this.text = null;
			this.attributes = Block.PLAIN;
		} else {
			int start = 0;
			int end = 0;
			LinkedBlock block = getFirst();
			while (block != null) {
				end = start + block.length();
				if (pos >= start && pos < end) {
					block.split(pos - start);
					return;
				}
				start = end;
				block = block.getNext();
			}
		}
	}

	@Override
	public void addBefore(Block block) {
		LinkedBlock lb = (LinkedBlock)block;
		lb.setParent(this.parent);
		lb.setPrev(this.prev);
		lb.setNext(this);
		this.setPrev(lb);
		if (parent.getFirst() == this) parent.setFirst(lb);
	}

	@Override
	public void addAfter(Block block) {
		LinkedBlock lb = (LinkedBlock)block;
		lb.setParent(this.parent);
		lb.setNext(this.next);
		lb.setPrev(this);
		this.setNext(lb);
	}

	@Override
	public void delete() {
		if (parent != null && parent.getFirst() == this) {
			parent.setFirst(this.next);
		}
		if (prev != null) {
			prev.setNext(this.next);
		}
		if (next != null) {
			next.setNext(this.prev);
		}
	}

	@Override
	public String present(Format format) {
		if (isLeaf()) {
			return format.format(text, attributes);
		}
		StringBuilder buf = new StringBuilder();
		LinkedBlock block = getFirst();
		while (block != null) {
			buf.append(block.present(format));
			block = block.getNext();
		}
		return buf.toString();
	}

	@Override
	public boolean isLeaf() {
		return first == null;
	}

	@Override
	public int length() {
		if (isLeaf()) {
			return text.length();
		}
		int ret = 0;
		LinkedBlock block = getFirst();
		while (block != null) {
			ret += block.length();
			block = block.getNext();
		}
		return ret;
	}
	
	@Override public String toString() {
		if (isLeaf()) {
			String s = "";
			if (hasAttribute(BOLD)) s += "B";
			if (hasAttribute(ITALIC)) s += "I";
			return s + "(" + text + ")";
		}
		StringBuilder buf = new StringBuilder("REC[");
		boolean had = false;
		LinkedBlock block = getFirst();
		while (block != null) {
			if (had) {
				buf.append(",");
			}
			buf.append(block.toString());
			had = true;
			block = block.getNext();
		}
		buf.append("]");
		return buf.toString();
	}
}
