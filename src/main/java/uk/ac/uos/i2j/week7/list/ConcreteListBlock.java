package uk.ac.uos.i2j.week7.list;

import java.util.ArrayList;
import java.util.List;

import uk.ac.uos.i2j.week7.Block;
import uk.ac.uos.i2j.week7.Format;

public class ConcreteListBlock implements ListBlock {
	private String text;
	private int attributes;
	protected List<ListBlock> blocks;
	protected ListBlock parent;
	
	public ConcreteListBlock(String text, int attributes, ListBlock parent) {
		this.text = text;
		this.attributes = attributes;
		this.parent = parent;
	}
	
	public ConcreteListBlock(String text, ListBlock parent) {
		this(text, Block.PLAIN, parent);
	}
	
	public ConcreteListBlock(String text) {
		this(text, Block.PLAIN, null);
	}
	
	public ConcreteListBlock() {
		this("");
	}
	
	public void setParent(ListBlock block) { parent = block; }
	
	@Override
	public void setAttribute(int attribute) {
		if (isLeaf()) {
			this.attributes |= attribute;
		} else {
			for (ListBlock block : blocks) {
				block.setAttribute(attribute);
			}
		}
	}

	@Override
	public void clearAttribute(int attribute) {
		if (isLeaf()) {
			this.attributes ^= attribute;
		} else {
			for (ListBlock block : blocks) {
				block.clearAttribute(attribute);
			}
		}
	}

	@Override
	public boolean hasAttribute(int attribute) {
		return 0 != (this.attributes & attribute);
	}
	
	@Override
	public Block findBlock(int pos) {
		if (pos < 0 || pos >= length()) return null;
		int start = 0;
		int end = 0;
		for (ListBlock block : blocks) {
			end = start + block.length();
			if (pos >= start && pos < end) {
				if (block.isLeaf()) {
					return block;
				}
				return block.findBlock(pos-start);
			}
			start = end;
		}
		return null;
	}

	@Override
	public void split(int pos) {
		if (pos < 0 || pos >= length()) return;
		if (isLeaf()) {
			blocks = new ArrayList<>();
			blocks.add(new ConcreteListBlock(this.text.substring(0, pos), this.attributes, this)); 
			blocks.add(new ConcreteListBlock(this.text.substring(pos), this.attributes, this));
			this.text = null;
			this.attributes = Block.PLAIN;
		} else {
			int start = 0;
			int end = 0;
			for (ListBlock block : blocks) {
				end = start + block.length();
				if (pos >= start && pos < end) {
					block.split(pos - start);
					return;
				}
				start = end;
			}
		}
	}

	@Override
	public void addBefore(Block block) {
		ListBlock lb = (ListBlock)block;
		lb.setParent(this.parent);
		if (isLeaf()) {
			blocks = new ArrayList<>();
			blocks.add(lb); 
			blocks.add(new ConcreteListBlock(this.text, this.attributes, this.parent));
			this.text = null;
			this.attributes = Block.PLAIN;
		} else {
			blocks.add(0, lb);
		}
	}

	@Override
	public void addAfter(Block block) {
		ListBlock lb = (ListBlock)block;
		lb.setParent(this.parent);
		if (isLeaf()) {
			blocks = new ArrayList<>();
			blocks.add(new ConcreteListBlock(this.text, this.attributes, this.parent));
			blocks.add(lb); 
			this.text = null;
			this.attributes = Block.PLAIN;
		} else {
			blocks.add(lb);
		}
	}

	@Override
	public void delete() {
		if (null == parent) {
			if (isLeaf()) {
				this.text = "";
				this.attributes = Block.PLAIN;
			} else {
				this.blocks = null;
			}
		} else {
			parent.remove(this);
		}
	}

	@Override
	public String present(Format format) {
		StringBuilder buf = new StringBuilder();
		if (isLeaf()) {
			return format.format(text, attributes);
		}
		for (ListBlock block : blocks) {
			buf.append(block.present(format));
		}
		return buf.toString();
	}

	@Override
	public boolean isLeaf() {
		return blocks == null;
	}

	@Override
	public int length() {
		if (isLeaf()) {
			return text.length();
		}
		int ret = 0;
		for (ListBlock block : blocks) {
			ret += block.length();
		}
		return ret;
	}
	
	@Override public String toString() {
		if (isLeaf()) {
			String s = "";
			if (hasAttribute(BOLD)) s += "B";
			if (hasAttribute(ITALIC)) s += "I";
			return s + "(" + text + ")";
		}
		StringBuilder buf = new StringBuilder("REC[");
		boolean had = false;
		for (ListBlock block : blocks) {
			if (had) {
				buf.append(",");
			}
			buf.append(block.toString());
			had = true;
		}
		buf.append("]");
		return buf.toString();
	}

	@Override
	public void remove(ListBlock child) {
		blocks.remove(child);
	}
}
