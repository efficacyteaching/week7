package uk.ac.uos.i2j.week7.linked;

import uk.ac.uos.i2j.week7.Block;

public interface LinkedBlock extends Block {
	void setParent(LinkedBlock block);
	LinkedBlock getParent();
	void setFirst(LinkedBlock block);
	LinkedBlock getFirst();
	void setPrev(LinkedBlock block);
	LinkedBlock getPrev();
	void setNext(LinkedBlock block);
	LinkedBlock getNext();
}
