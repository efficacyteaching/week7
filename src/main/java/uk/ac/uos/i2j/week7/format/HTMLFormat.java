package uk.ac.uos.i2j.week7.format;

import uk.ac.uos.i2j.week7.Block;
import uk.ac.uos.i2j.week7.Format;

public class HTMLFormat implements Format {

	@Override
	public String format(String text, int attributes) {
		if ((attributes & Block.BOLD) > 0 && (attributes & Block.ITALIC) > 0) {
			return "<b><i>" + text + "</i></b>";
		}
		if ((attributes & Block.BOLD) > 0) {
			return "<b>" + text + "</b>";
		}
		if ((attributes & Block.ITALIC) > 0) {
			return "<i>" + text + "</i>";
		}
		return text;
	}

}
