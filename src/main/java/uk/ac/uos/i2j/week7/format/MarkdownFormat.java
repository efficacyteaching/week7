package uk.ac.uos.i2j.week7.format;

import uk.ac.uos.i2j.week7.Block;
import uk.ac.uos.i2j.week7.Format;

public class MarkdownFormat implements Format {

	@Override
	public String format(String text, int attributes) {
		if ((attributes & Block.BOLD) > 0 && (attributes & Block.ITALIC) > 0) {
			return "**_" + text + "_**";
		}
		if ((attributes & Block.BOLD) > 0) {
			return "**" + text + "**";
		}
		if ((attributes & Block.ITALIC) > 0) {
			return "_" + text + "_";
		}
		return text;
	}

}
