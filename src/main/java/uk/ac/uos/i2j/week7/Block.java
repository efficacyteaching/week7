package uk.ac.uos.i2j.week7;

public interface Block {
	static int PLAIN = 0;
	static int ITALIC = 1;
	static int BOLD = 2;

	void setAttribute(int attribute);
	void clearAttribute(int attribute);
	boolean hasAttribute(int attribute);
	
	void split(int pos);
	void addBefore(Block block);
	void addAfter(Block block);
	void delete();
	
	Block findBlock(int pos);
	boolean isLeaf();
	int length();
	
	String present(Format format);
}
