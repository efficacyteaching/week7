package uk.ac.uos.i2j.week7;

public interface Format {
	String format(String text, int attributes);
}
