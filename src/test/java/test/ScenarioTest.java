package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import uk.ac.uos.i2j.week7.Block;
import uk.ac.uos.i2j.week7.format.HTMLFormat;
import uk.ac.uos.i2j.week7.format.MarkdownFormat;
import uk.ac.uos.i2j.week7.format.PlainFormat;
import uk.ac.uos.i2j.week7.linked.ConcreteLinkedBlock;
import uk.ac.uos.i2j.week7.list.ConcreteListBlock;

interface BlockFactory {
	public Block make(String text);
}

public class ScenarioTest {
	private void runProvidedScenario(BlockFactory fac) {
		Block block = fac.make("Now is the time for a party.");
		block.split(21);
		block.split(20);
		block.findBlock(20).delete();
		block.findBlock(19).addAfter(fac.make("all good men to come to the aid of the"));
		block.split(24);
		block.split(29);
		block.findBlock(20).setAttribute(Block.BOLD);
		block.findBlock(24).setAttribute(Block.ITALIC);

		assertEquals("Now is the time for all good men to come to the aid of the party.",
				block.present(new PlainFormat()));
		assertEquals("Now is the time for **all **_good _men to come to the aid of the party.",
				block.present(new MarkdownFormat()));
		assertEquals("Now is the time for <b>all </b><i>good </i>men to come to the aid of the party.",
				block.present(new HTMLFormat()));
	}

	@Test public void testProvidedScenarioUsingListBlocks() {
		runProvidedScenario(new BlockFactory() {
			@Override
			public Block make(String text) {
				return new ConcreteListBlock(text);
			}
		});
	}

	@Test public void testProvidedScenarioUsingLinkedBlocks() {
		runProvidedScenario(new BlockFactory() {
			@Override
			public Block make(String text) {
				return new ConcreteLinkedBlock(text);
			}
		});
	}
}
